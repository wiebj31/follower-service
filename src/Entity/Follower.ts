import { Entity, PrimaryGeneratedColumn, Column } from "typeorm";

@Entity()
export class Follower {

    constructor(userId:string, followerId: string){
        this.userId = userId;
        this.followerId = followerId
        this.startedFollowing = new Date();
    }
    @PrimaryGeneratedColumn("uuid")
    id: string;

    @Column()
    userId!: string;

    @Column()
    followerId!: string;

    @Column('datetime')
    startedFollowing!: Date;
}

export class FollowerRelation{
    constructor(userId:string, followerId: string){
        this.userId = userId;
        this.followerId = followerId
    }

    userId!: string;
    followerId!: string;
}