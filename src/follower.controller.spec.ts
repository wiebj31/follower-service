import { Test, TestingModule } from '@nestjs/testing';
import { FollowerController } from './follower.controller';
import { FollowerService } from './follower.service';

describe('AppController', () => {
  let appController: FollowerController;

  beforeEach(async () => {
    const app: TestingModule = await Test.createTestingModule({
      controllers: [FollowerController],
      providers: [FollowerService],
    }).compile();

    appController = app.get<FollowerController>(FollowerController);
  });

  
});
