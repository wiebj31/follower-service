import { Controller, Delete, Get, Param, Post } from '@nestjs/common';
import { get } from 'http';
import { FollowerService } from './follower.service';
import { FollowerRelation, Follower } from './Entity/Follower';
import { Body } from '@nestjs/common';

@Controller()
export class FollowerController {
  constructor(private readonly followerService: FollowerService) {}

  @Get('/follower/:userId')
  getByUserId(@Param('userId') userId: string){
    return this.followerService.getByUserId(userId);
  }

  @Get('/follower_count/:userId')
  getcCountByUserId(@Param('userId') userId: string){
    return this.followerService.getcCountByUserId(userId);
  }

  @Post('/follower')
  createFollowerRelationship(@Body() follower: FollowerRelation){
    this.followerService.createFollowerRelation(follower);
  }

  @Delete('/follower/:userId/:followerId')
  deleteFollowerRelationship(@Param('userId') userId: string,@Param('followerId') followerId: string){
    return this.followerService.deleteFollowerRelation(userId, followerId);
  }
}
