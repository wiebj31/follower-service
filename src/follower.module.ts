import { Module } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';
import { ClientsModule, Transport } from '@nestjs/microservices';
import { TypeOrmModule } from '@nestjs/typeorm';
import { FollowerController } from './follower.controller';
import { FollowerService } from './follower.service';
import { Follower } from './Entity/Follower';

@Module({
  imports: [ClientsModule.register([
    {
      name: 'FOLLOWER_SERVICE',
      transport: Transport.REDIS,
      options: {
        url: 'redis://localhost:6379',
        auth_pass: 'redispassword'
      }
    },
  ]),
  ConfigModule.forRoot({isGlobal:true}),
  TypeOrmModule.forRoot({
    type: 'mysql',
    host: '178.84.152.77',
    port: 3309,
    username: 'root',
    password: 'wachtwoord',
    database: 'Follower',
    entities: [Follower],
    synchronize: true,
  }),
  
  TypeOrmModule.forFeature([Follower])],
  controllers: [FollowerController],
  providers: [FollowerService],
})
export class AppModule {}
