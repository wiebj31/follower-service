import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { FollowerRelation, Follower } from './Entity/Follower';

@Injectable()
export class FollowerService {
constructor(@InjectRepository(Follower)
private followerRepository: Repository<Follower>){

}

  async createFollowerRelation(follower: FollowerRelation){
    return this.followerRepository.save(follower);
  }

  async deleteFollowerRelation(userId: string, followerId: string){
    return await this.followerRepository.findOne({userId: userId, followerId: followerId}).then(async res=> {
      if(res){
        return await this.followerRepository.remove(res);
      }
      else{
        return 'haha sukkel'
      }
    });
  }

  async getByUserId(userId: string){
    return await this.followerRepository.find({ userId: userId}).then(res =>{
      if(res){
        return res
      }
      else{
        return 'haha sukkel'
      }
    })
  }

  async getcCountByUserId(userId: string){
    const count = await this.followerRepository.count({ userId: userId})
    return count;
  }

}
